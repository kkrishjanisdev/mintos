<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Account;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiCaseTest extends WebTestCase
{
    public function testGetAllAccounts(): void
    {
        $client = static::createClient();
        $client->request('GET', sprintf('/api/v1/accounts'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertIsArray($responseData);
    }

    public function testGetAccounts(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $id = 2;
        $client->request('GET', sprintf('/api/v1/client/accounts/?id=%d', $id));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('status', $responseData);
        $this->assertEquals(200, $responseData['status']);

        $this->assertArrayHasKey('results', $responseData);
        $this->assertArrayHasKey('total_results', $responseData);
    }

    public function testTransferFunds(): void
    {
        $client = static::createClient();

        $entityManager = $client->getContainer()->get('doctrine')->getManager();

        $account1 = $entityManager->getRepository(Account::class)->findOneBy(['client' => '2'], ['balance' => 'DESC']);
        $account2 = $entityManager->getRepository(Account::class)->findOneBy(['client' => '2'], ['balance' => 'ASC']);

        $client->request('POST', '/api/v1/transfer-funds?sourceAccountId=' . $account1->getId() . '&destinationAccountId=' . $account2->getId() . '&amount=1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('message', $responseData);
        $this->assertEquals('Funds transferred successfully', $responseData['message']);
    }

    public function testGetTransactionHistory(): void
    {
        $client = static::createClient();

        $entityManager = $client->getContainer()->get('doctrine')->getManager();

        $account = $entityManager->getRepository(Account::class)->findOneBy(['client' => '2'], ['balance' => 'DESC']);

        $client->request('GET', '/api/v1/transaction-history?accountId=', ['accountId' => $account->getId()]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('status', $responseData);
        $this->assertSame(JsonResponse::HTTP_OK, $responseData['status']);

        $this->assertArrayHasKey('page', $responseData);
        $this->assertSame(1, $responseData['page']);

        $this->assertArrayHasKey('results_per_page', $responseData);
        $this->assertSame(2, $responseData['results_per_page']);

        $this->assertArrayHasKey('results', $responseData);

        $this->assertArrayHasKey('total_results', $responseData);
    }
}
