## Getting Started

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/) (v2.10+)
2. Run `make build` to build images
3. Run `make up` to run project
4. After running `make up` wait until it installs all dependencies. (Can see the output in Docker PHP container). After all dependencies are installed - run `make sh` to open PHP container sh and execute further commands
5. When sh is active - Run `php bin/console make:migration`, `php bin/console doctrine:migrations:migrate` to migrate database
6. Run `php bin/console doctrine:fixtures:load` to load fake data inside database
7. Open `https://localhost` in your favorite web browser and [accept the auto-generated TLS certificate](https://stackoverflow.com/a/15076602/1352334)
8. Run `php bin/phpunit` to test project
9. Run `make down` to stop the Docker containers.

## API endpoints

You will need _Postman_ or other API platform

Available endpoints:

1. All accounts - _https://localhost/api/v1/accounts_

-   No parametres needed

2. Money tranfer between accounts - _https://localhost/api/v1/transfer-funds_

-   Parametres:
    ['key' => sourceAccountId, 'value' => uuid],
    ['key' => destinationAccountId, 'value' => uuid],
    ['key' => amount, 'value' => int (2 digits after the decimal point)]

3. View client accounts - _https://localhost/api/v1/client/accounts_

-   Parametres:
    ['key' => id, 'value' => Client ID]

4. Transaction history - _https://localhost/api/v1/transaction-history_

-   Parametres:
    ['key' => accountId, 'value' => Account UUID]
-   Optional parametres:
    ['key' => page, 'value' => int, 'default' => 1]

**Enjoy!**
