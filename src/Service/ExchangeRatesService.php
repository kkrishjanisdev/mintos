<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExchangeRatesService
{
    public function convertAmount($sourceAccount, $destinationAccount, $amount)
    {
        $httpClient = HttpClient::create();
        $apiKey = "087f7fa8a02b7b3c7613d1cdc4d17a71";
        $symbols = "USD, GBP, EUR";
        $apiUrl = "http://api.exchangeratesapi.io/v1/latest?access_key={$apiKey}&symbols={$symbols}";


        // GET exchange rates
        $response = $httpClient->request('GET', $apiUrl);
        $data = $response->toArray();

        $currencyFrom = $sourceAccount->getCurrencies()->getName();
        $currencyTo = $destinationAccount->getCurrencies()->getName();

        // Check if our needed currency exist in data array
        if (!empty($data["rates"]) && array_key_exists($currencyFrom, $data["rates"])) {
            $amountToEUR = $amount;
            // If currency base are not the same as our currency from then need convert to EUR
            if (!empty($data["base"]) && $data["base"] != $currencyFrom) {
                $amountToEUR = $amount / $data["rates"][$currencyFrom];
            }
            // Convert from EUR to destination account currency
            return round($amountToEUR * $data["rates"][$currencyTo], 2);
        } else {
            return new JsonResponse(['error' => 'Currency rate not found'], 404);
        }
    }
}
