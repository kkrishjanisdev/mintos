<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Account;
use App\Entity\TransactionHistory;
use DateTime;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\AccountRepository;
use App\Repository\TransactionHistoryRepository;
use App\Service\ExchangeRatesService;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function transferFunds(Request $request, ExchangeRatesService $exchangeRatesService): JsonResponse
    {
        $sourceAccountId = $request->query->get('sourceAccountId');
        $destinationAccountId = $request->query->get('destinationAccountId');
        $amount = $request->query->get('amount');

        try {
            // Retrieve accounts from database
            $sourceAccount = $this->entityManager->getRepository(Account::class)->find($sourceAccountId);
            $destinationAccount = $this->entityManager->getRepository(Account::class)->find($destinationAccountId);

            // Check if source account exists
            if (!$sourceAccount) {
                return new JsonResponse(['error' => 'Source account not found'], 404);
            }

            // Check if destination account exists
            if (!$destinationAccount) {
                return new JsonResponse(['error' => 'Destination account not found'], 404);
            }

            // Check if amount is provided or amount contains numbers
            if (!$amount || !is_numeric($amount)) {
                return new JsonResponse(['error' => 'Amount is not provided or provided incorrectly'], 404);
            }

            // Check if source and destination accounts are not identical
            if ($sourceAccountId == $destinationAccountId) {
                return new JsonResponse(['error' => 'Can`t send money to the same account'], 400);
            }

            // Check if has sufficient funds
            if (!$sourceAccount->hasSufficientFunds($amount)) {
                return new JsonResponse(['error' => 'Insufficient funds'], 400);
            }

            $sourceAccount->setBalance($sourceAccount->getBalance() - $amount);

            $finalAmount = $amount;

            // If currency between accounts are different then need to convert amount
            if ($sourceAccount->getCurrencies()->getId() != $destinationAccount->getCurrencies()->getId()) {
                try {
                    $finalAmount = $exchangeRatesService->convertAmount($sourceAccount, $destinationAccount, $amount);
                } catch (\Exception $e) {
                    return new JsonResponse(['error' => 'Service is unavailable at the moment'], 500);
                }
            }

            $destinationAccount->setBalance($destinationAccount->getBalance() + $finalAmount);

            // Let`s make transaction history record
            $transactionHistory = new TransactionHistory();
            $transactionHistory->setFromAccount($sourceAccount);
            $transactionHistory->setToAccount($destinationAccount);
            $transactionHistory->setSentAmount($amount);
            $transactionHistory->setRecievedAmount($finalAmount);
            $transactionHistory->setDate(new DateTime());
            $transactionHistory->setFromCurrency($sourceAccount->getCurrencies());
            $transactionHistory->setToCurrency($destinationAccount->getCurrencies());
            $this->entityManager->persist($transactionHistory);

            $this->entityManager->flush();

            return new JsonResponse(['message' => 'Funds transferred successfully']);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }

    public function getAccounts(Request $request, AccountRepository $accountRepository): JsonResponse
    {
        $id = $request->query->get('id');

        try {
            $results = $accountRepository->findByClientId($id);
            $totalResults = count($results);

            if (!$results) {
                return new JsonResponse(['error' => 'Client not found or don`t have account'], 404);
            }

            return new JsonResponse([
                'status' => JsonResponse::HTTP_OK,
                'results' => $results,
                'total_results' => $totalResults
            ]);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }

    public function getAllAccounts(AccountRepository $accountRepository): JsonResponse
    {
        try {
            $results = $accountRepository->getAllAccounts();

            $accounts = [];
            if (!$results) {
                return new JsonResponse(['error' => 'Accounts not found'], 404);
            } else {
                foreach ($results as $result) {
                    $accounts[] = [
                        "id" => $result->id,
                        "balance" => $result->balance,
                        "currency" => $result->getCurrencies()->getName()
                    ];
                }
            }

            return new JsonResponse($accounts);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }

    public function getTransactionHistory(Request $request, PaginatorInterface $paginator, TransactionHistoryRepository $transactionHistory): JsonResponse
    {
        $page = $request->query->get('page', 1);
        $accountId = $request->query->get('accountId');

        if (!$accountId) {
            return new JsonResponse(['error' => 'Account not provided'], 400);
        }

        try {
            // Define how much records will show per page
            $perPage = 2;

            $queryBuilder = $transactionHistory->createQueryBuilder('t');
            $queryBuilder
                ->select('t')
                ->where('t.fromAccount = :accountId')
                ->orWhere('t.toAccount = :accountId')
                ->orderBy('t.date', 'DESC')
                ->setParameter('accountId', $accountId);

            $pagination = $paginator->paginate(
                $queryBuilder,
                $page,
                $perPage
            );

            $results = $pagination->getItems();
            $totalResults = count($queryBuilder->getQuery()->getResult());

            return new JsonResponse([
                'status' => JsonResponse::HTTP_OK,
                'page' => $page,
                'results_per_page' => $perPage,
                'results' => $results,
                'total_results' => $totalResults
            ]);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }
}
