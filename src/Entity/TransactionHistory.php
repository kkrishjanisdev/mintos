<?php

namespace App\Entity;

use App\Repository\TransactionHistoryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TransactionHistoryRepository::class)]
class TransactionHistory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    public ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    public ?\DateTimeInterface $date = null;

    #[ORM\ManyToOne(inversedBy: 'transactionHistories')]
    #[ORM\JoinColumn(nullable: false)]
    public ?Account $fromAccount = null;

    #[ORM\ManyToOne(inversedBy: 'transactionHistoriesTo')]
    #[ORM\JoinColumn(nullable: false)]
    public ?Account $toAccount = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    public ?string $sentAmount = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    public ?string $recievedAmount = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    public ?Currencies $fromCurrency = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    public ?Currencies $toCurrency = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getFromAccount(): ?Account
    {
        return $this->fromAccount;
    }

    public function setFromAccount(?Account $fromAccount): static
    {
        $this->fromAccount = $fromAccount;

        return $this;
    }

    public function getSentAmount(): ?string
    {
        return $this->sentAmount;
    }

    public function setSentAmount(string $sentAmount): static
    {
        $this->sentAmount = $sentAmount;

        return $this;
    }

    public function getRecievedAmount(): ?string
    {
        return $this->recievedAmount;
    }

    public function setRecievedAmount(string $recievedAmount): static
    {
        $this->recievedAmount = $recievedAmount;

        return $this;
    }

    public function getFromCurrency(): ?Currencies
    {
        return $this->fromCurrency;
    }

    public function setFromCurrency(?Currencies $fromCurrency): static
    {
        $this->fromCurrency = $fromCurrency;

        return $this;
    }

    public function getToCurrency(): ?Currencies
    {
        return $this->toCurrency;
    }

    public function setToCurrency(?Currencies $toCurrency): static
    {
        $this->toCurrency = $toCurrency;

        return $this;
    }

    public function getToAccount(): ?Account
    {
        return $this->toAccount;
    }

    public function setToAccount(?Account $toAccount): static
    {
        $this->toAccount = $toAccount;

        return $this;
    }
}
