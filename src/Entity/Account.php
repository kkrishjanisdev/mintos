<?php

namespace App\Entity;

use App\Repository\AccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

use function PHPSTORM_META\type;

#[ORM\Entity(repositoryClass: AccountRepository::class)]
class Account
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    public ?Uuid $id;

    public function __construct()
    {
        $this->id = Uuid::v1();
        $this->transactionHistories = new ArrayCollection();
        $this->transactionHistoriesTo = new ArrayCollection();
    }

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2, options: ["default" => 0])]
    #[Assert\PositiveOrZero]
    public ?string $balance;

    #[ORM\ManyToOne(inversedBy: 'Account')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Client $client = null;

    #[ORM\OneToMany(mappedBy: 'fromAccount', targetEntity: TransactionHistory::class, orphanRemoval: true)]
    private Collection $transactionHistories;

    #[ORM\ManyToOne(inversedBy: 'toAccount')]
    #[ORM\JoinColumn(nullable: true)]
    private ?TransactionHistory $transactionHistory = null;

    #[ORM\OneToMany(mappedBy: 'toAccount', targetEntity: TransactionHistory::class, orphanRemoval: true)]
    private Collection $transactionHistoriesTo;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    public ?Currencies $Currencies = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getBalance(): ?string
    {
        return $this->balance;
    }

    public function setBalance(string $balance): static
    {
        $this->balance = $balance;

        return $this;
    }

    public function hasSufficientFunds($amount)
    {
        return $this->getBalance() > $amount;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): static
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection<int, TransactionHistory>
     */
    public function getTransactionHistories(): Collection
    {
        return $this->transactionHistories;
    }

    public function addTransactionHistory(TransactionHistory $transactionHistory): static
    {
        if (!$this->transactionHistories->contains($transactionHistory)) {
            $this->transactionHistories->add($transactionHistory);
            $transactionHistory->setFromAccount($this);
        }

        return $this;
    }

    public function removeTransactionHistory(TransactionHistory $transactionHistory): static
    {
        if ($this->transactionHistories->removeElement($transactionHistory)) {
            // set the owning side to null (unless already changed)
            if ($transactionHistory->getFromAccount() === $this) {
                $transactionHistory->setFromAccount(null);
            }
        }

        return $this;
    }

    public function getTransactionHistory(): ?TransactionHistory
    {
        return $this->transactionHistory;
    }

    public function setTransactionHistory(?TransactionHistory $transactionHistory): static
    {
        $this->transactionHistory = $transactionHistory;

        return $this;
    }

    /**
     * @return Collection<int, TransactionHistory>
     */
    public function getTransactionHistoriesTo(): Collection
    {
        return $this->transactionHistoriesTo;
    }

    public function addTransactionHistoriesTo(TransactionHistory $transactionHistoriesTo): static
    {
        if (!$this->transactionHistoriesTo->contains($transactionHistoriesTo)) {
            $this->transactionHistoriesTo->add($transactionHistoriesTo);
            $transactionHistoriesTo->setToAccount($this);
        }

        return $this;
    }

    public function removeTransactionHistoriesTo(TransactionHistory $transactionHistoriesTo): static
    {
        if ($this->transactionHistoriesTo->removeElement($transactionHistoriesTo)) {
            // set the owning side to null (unless already changed)
            if ($transactionHistoriesTo->getToAccount() === $this) {
                $transactionHistoriesTo->setToAccount(null);
            }
        }

        return $this;
    }

    public function getCurrencies(): ?Currencies
    {
        return $this->Currencies;
    }

    public function setCurrencies(?Currencies $Currencies): static
    {
        $this->Currencies = $Currencies;

        return $this;
    }
}
