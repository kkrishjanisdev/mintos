<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Currencies;

class CurrencieFixture extends Fixture
{
    public const CURRENCY = 'currency';

    public function load(ObjectManager $manager): void
    {
        $currencies = [
            [
                'name' => 'USD',
                'symbol' => '$'
            ],
            [
                'name' => 'GBP',
                'symbol' => '£'
            ],
            [
                'name' => 'EUR',
                'symbol' => '€'
            ]
        ];

        foreach ($currencies as $currency) {
            $curr = new Currencies();
            $curr->setName($currency['name']);
            $curr->setSymbol($currency['symbol']);
            $manager->persist($curr);
        }

        $manager->flush();
        $this->setReference(self::CURRENCY, $curr);
    }
}
