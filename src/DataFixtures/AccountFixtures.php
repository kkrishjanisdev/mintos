<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Account;
use App\Entity\Currencies;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;

class AccountFixtures extends Fixture implements DependentFixtureInterface
{
    private EntityManagerInterface $entityManager;

    public const ACCOUNT_REFERENCE = 'account';

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function load(ObjectManager $manager): void
    {
        $repository = $this->entityManager->getRepository(Currencies::class);

        // Create Client Accounts with balance > 0
        $account = new Account();
        $account->setClient($this->getReference(ClientFixtures::CLIENT_REFERENCE));
        $account->setCurrencies($this->getReference(CurrencieFixture::CURRENCY));
        $account->setBalance(rand(100, 100000));
        $manager->persist($account);

        // Create Client Account with balance = 0
        $account2 = new Account();
        $account2->setClient($this->getReference(ClientFixtures::CLIENT_REFERENCE));
        $account2->setCurrencies($this->getReference(CurrencieFixture::CURRENCY));
        $account2->setBalance(0);
        $manager->persist($account2);

        // Create Client Account with balance > 0 and different currency
        $USD = $repository->findOneBy(['name' => 'USD']);
        $account3 = new Account();
        $account3->setClient($this->getReference(ClientFixtures::CLIENT_REFERENCE));
        $account3->setCurrencies($USD);
        $account3->setBalance(150.30);
        $manager->persist($account3);

        // Create Client Account with different currency
        $GBP = $repository->findOneBy(['name' => 'GBP']);
        $account4 = new Account();
        $account4->setClient($this->getReference(ClientFixtures::CLIENT_REFERENCE));
        $account4->setCurrencies($GBP);
        $account4->setBalance(20.34);
        $manager->persist($account4);

        $manager->flush();
        $this->setReference(self::ACCOUNT_REFERENCE, $account4);
    }

    public function getDependencies()
    {
        return [
            ClientFixtures::class,
            CurrencieFixture::class
        ];
    }
}
