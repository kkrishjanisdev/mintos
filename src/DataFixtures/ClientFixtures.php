<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Client;

class ClientFixtures extends Fixture
{
    public const CLIENT_REFERENCE = 'client';

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 2; $i++) {
            $client = new Client();
            $client->setName('Client_' . $i);
            $manager->persist($client);
        }

        $manager->flush();
        $this->setReference(self::CLIENT_REFERENCE, $client);
    }
}
