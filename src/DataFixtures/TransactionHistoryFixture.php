<?php

namespace App\DataFixtures;

use App\Entity\TransactionHistory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Account;
use DateTime;

class TransactionHistoryFixture extends Fixture implements DependentFixtureInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function load(ObjectManager $manager): void
    {
        $account = $this->entityManager->getRepository(Account::class)->findOneBy(['client' => '2'], ['balance' => 'DESC']);

        $history = new TransactionHistory();
        $history->setFromAccount($this->getReference(AccountFixtures::ACCOUNT_REFERENCE));
        $history->setToAccount($account);
        $history->setFromCurrency($this->getReference(AccountFixtures::ACCOUNT_REFERENCE)->getCurrencies());
        $history->setToCurrency($account->getCurrencies());
        $history->setDate(new DateTime());
        $history->setSentAmount(1);
        $history->setRecievedAmount(1.14);

        $manager->persist($history);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AccountFixtures::class
        ];
    }
}
