<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231116123512 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE client_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE currencies_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE transaction_history_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE account (id UUID NOT NULL, client_id INT NOT NULL, transaction_history_id INT DEFAULT NULL, currencies_id INT NOT NULL, balance NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7D3656A419EB6921 ON account (client_id)');
        $this->addSql('CREATE INDEX IDX_7D3656A4F193B3E3 ON account (transaction_history_id)');
        $this->addSql('CREATE INDEX IDX_7D3656A4A33E2D84 ON account (currencies_id)');
        $this->addSql('COMMENT ON COLUMN account.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE client (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE currencies (id INT NOT NULL, name VARCHAR(3) NOT NULL, symbol VARCHAR(1) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE transaction_history (id INT NOT NULL, from_account_id UUID NOT NULL, to_account_id UUID NOT NULL, from_currency_id INT NOT NULL, to_currency_id INT NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, sent_amount NUMERIC(10, 2) NOT NULL, recieved_amount NUMERIC(10, 2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_51104CA9B0CF99BD ON transaction_history (from_account_id)');
        $this->addSql('CREATE INDEX IDX_51104CA9BC58BDC7 ON transaction_history (to_account_id)');
        $this->addSql('CREATE INDEX IDX_51104CA9A66BB013 ON transaction_history (from_currency_id)');
        $this->addSql('CREATE INDEX IDX_51104CA916B7BF15 ON transaction_history (to_currency_id)');
        $this->addSql('COMMENT ON COLUMN transaction_history.from_account_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN transaction_history.to_account_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A419EB6921 FOREIGN KEY (client_id) REFERENCES client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A4F193B3E3 FOREIGN KEY (transaction_history_id) REFERENCES transaction_history (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A4A33E2D84 FOREIGN KEY (currencies_id) REFERENCES currencies (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction_history ADD CONSTRAINT FK_51104CA9B0CF99BD FOREIGN KEY (from_account_id) REFERENCES account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction_history ADD CONSTRAINT FK_51104CA9BC58BDC7 FOREIGN KEY (to_account_id) REFERENCES account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction_history ADD CONSTRAINT FK_51104CA9A66BB013 FOREIGN KEY (from_currency_id) REFERENCES currencies (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction_history ADD CONSTRAINT FK_51104CA916B7BF15 FOREIGN KEY (to_currency_id) REFERENCES currencies (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE client_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE currencies_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE transaction_history_id_seq CASCADE');
        $this->addSql('ALTER TABLE account DROP CONSTRAINT FK_7D3656A419EB6921');
        $this->addSql('ALTER TABLE account DROP CONSTRAINT FK_7D3656A4F193B3E3');
        $this->addSql('ALTER TABLE account DROP CONSTRAINT FK_7D3656A4A33E2D84');
        $this->addSql('ALTER TABLE transaction_history DROP CONSTRAINT FK_51104CA9B0CF99BD');
        $this->addSql('ALTER TABLE transaction_history DROP CONSTRAINT FK_51104CA9BC58BDC7');
        $this->addSql('ALTER TABLE transaction_history DROP CONSTRAINT FK_51104CA9A66BB013');
        $this->addSql('ALTER TABLE transaction_history DROP CONSTRAINT FK_51104CA916B7BF15');
        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE currencies');
        $this->addSql('DROP TABLE transaction_history');
    }
}
